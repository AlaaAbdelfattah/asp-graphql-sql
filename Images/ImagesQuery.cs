using GraphQL.Types;
using GraphQLNetExample.EntityFramework;

namespace GraphQLNetExample.Images;

public class ImagesQuery : ObjectGraphType
{
    public ImagesQuery()
    {
        Field<ListGraphType<ImageType>>("images", resolve: context =>
        {
            var imagesContext = context.RequestServices.GetRequiredService<ImagesContext>();
            return imagesContext.Images.ToList();
        }
        );
    }
}
