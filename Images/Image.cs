using System.ComponentModel.DataAnnotations;
using GraphQL.Upload.AspNetCore;

namespace GraphQLNetExample.Images;

public class Image
{
    public Guid Id { get; set; }
    [Required]

    public string File { get; set; }
}
