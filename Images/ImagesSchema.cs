using GraphQL.Types;

namespace GraphQLNetExample.Images;

public class ImagesSchema : Schema
{
    public ImagesSchema(IServiceProvider serviceProvider) : base(serviceProvider)
    {
        Query = serviceProvider.GetRequiredService<ImagesQuery>();
        Mutation = serviceProvider.GetRequiredService<ImagesMutation>();
    }
}
