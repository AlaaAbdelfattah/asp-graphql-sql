using GraphQL.Types;

namespace GraphQLNetExample.Images;

public class ImageType : ObjectGraphType<Image>
{
  public ImageType()
  {
    Name = "Image";
    Description = "Image Type";
    Field(d => d.Id, nullable: false).Description("Image Id");
    Field(d => d.File, nullable: true).Description("Image File");
  }
}
