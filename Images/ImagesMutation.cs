using GraphQL;
using GraphQL.Types;
using GraphQL.Upload.AspNetCore;
using GraphQLNetExample.EntityFramework;

namespace GraphQLNetExample.Images
{
    public class ImagesMutation : ObjectGraphType
    {
        public ImagesMutation()
        {
            Field<StringGraphType>(
                "singleUpload",
                arguments: new QueryArguments(
                    new QueryArgument<UploadGraphType> { Name = "file" }
                ),
                resolve: context =>
               {

                   var Image = new Image();

                   var file = context.GetArgument<IFormFile>("file");
                   var ImagesContext = context.RequestServices.GetRequiredService<ImagesContext>();

                   using (var memoryStream = new MemoryStream())
                   {
                       file.CopyTo(memoryStream);
                       var fileBytes = memoryStream.ToArray();
                       string base64String = Convert.ToBase64String(fileBytes);
                       Image.File = base64String;
                   }
                   ImagesContext.Images.Add(Image);
                   ImagesContext.SaveChanges();

                   return Image.Id;
               }
            );
        }
    }
}
