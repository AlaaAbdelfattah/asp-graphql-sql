using GraphQLNetExample.Images;
using Microsoft.EntityFrameworkCore;

namespace GraphQLNetExample.EntityFramework
{
    public class ImagesContext : DbContext
    {
        public DbSet<Image> Images { get; set; }

        public ImagesContext(DbContextOptions options) : base(options)
        {
            
        }
    }
}
